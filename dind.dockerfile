FROM docker:19.03

RUN apk add --no-cache bash make git py-pip gcc build-base python3-dev libffi-dev openssl-dev

ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

RUN pip install --no-cache-dir docker-compose
