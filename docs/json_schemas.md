# DataFrames and JSON Schemas

If your app uses Pandas [DataFrame](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html) to work with table-like data, at some point you may want to convert it to JSON in order to use it in a response.
Also, you may want to be able to accept table-like data from user and convert it into a DataFrame.
This page describes tools you can use to develop an application that can do it.


## DataFrame to JSON

Let's imagine that you have dataframe `df` that looks like this:

|   | Module | Catalogue ID |         Name |  Qnt |  UOM | Unit_cost | Total_cost |
| - | ------ | ------------ | ------------ | ---- | ---- | --------- | ---------- |
| 0 |     SK | catalogue_id | mooring line |  135 | n.d. |     85.50 |    11542.5 |
| 1 |     SK | catalogue_id |        chain | 1685 | n.d. |    302.76 |   510150.6 |
| 2 |     SK | catalogue_id |       anchor |    1 | n.d. |   8918.00 |     8918.0 |
| 3 |     SK | catalogue_id |       anchor |    2 | n.d. |  57624.00 |   115248.0 |
| 4 |     SK | catalogue_id |         buoy |   10 | n.d. |   6000.00 |    60000.0 |
| 5 |     SK | catalogue_id |      shackle |  100 | n.d. |   1023.00 |   102300.0 |

We recommend using [`to_dict('list')`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_dict.html) because it generates data compatible with DataFrame's own constructor (see next section).

```python
document = df.to_dict('list')
```

The code above will produce a `dict` that can be then passed to Flask's [`jsonify()`](https://flask.palletsprojects.com/en/latest/api/#flask.json.jsonify) or embedded into more complex data structures. If all you need is to immediately serialize DataFrame to a string in JSON format, just wrap the code with [`json.dumps()`](https://docs.python.org/3/library/json.html#json.dumps):

```python
json_string = json.dumps(df.to_dict('list'))
```

The result looks something like this:
```json
{
  "Module": ["SK","SK","SK","SK","SK","SK"],
  "Catalogue ID": ["catalogue_id","catalogue_id","catalogue_id","catalogue_id","catalogue_id","catalogue_id"],
  "Name": ["mooring line","chain","anchor","anchor","buoy","shackle"],
  "Qnt": [135,1685,1,2,10,100],
  "UOM": ["n.d.","n.d.","n.d.","n.d.","n.d.","n.d."],
  "Unit_cost": [85.5,302.76,8918.0,57624.0,6000.0,1023.0],
  "Total_cost": [11542.5,510150.6,8918.0,115248.0,60000.0,102300.0]
}
```


## JSON to DataFrame

If you have a dictionary like the one that was generated in the previous example and you want to create a DataFrame from it, just pass the dictionary to the constructor:

```python
df = DataFrame(document)
```


## Validate JSON

If your application expects user to send table-like data in request or the app itself sends such data in response, it means that the format needs to be declared in the JSON Schema of that application. Pandas has function [`build_table_schema()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.io.json.build_table_schema.html) that generated schema for given DataFrame, but unfortunately that is not a [JSON Schema](https://json-schema.org/), it is some custom Pandas' schema format.

Our library provides function `schema_from_document()` that builds upon the Pandas function but converts it to a proper JSON Schema.

For the example above, the result will look like this:

```json
{
  "type": "object",
  "additionalProperties": false,
  "properties": {
    "index": {
      "type": "array",
      "items": {
        "type": "integer"
      }
    },
    "Module": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "Catalogue ID": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "Name": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "Qnt": {
      "type": "array",
      "items": {
        "type": "integer"
      }
    },
    "UOM": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "Unit_cost": {
      "type": "array",
      "items": {
        "type": "number"
      }
    },
    "Total_cost": {
      "type": "array",
      "items": {
        "type": "number"
      }
    }
  }
}
```

Also, if dtop_shared_library is installed and you have development dependencies installed in your environment, you can use command-line utility `schema_from_document` to produce the same schema but in form of YAML, which means you can copy and paste it into your OpenAPI schema.

```bash
pipenv install --dev
pipenv install -e .
pipenv run schema_from_document example.json
```
```yaml
type: object
additionalProperties: false
properties:
  index:
    type: array
    items:
      type: integer
  Module:
    type: array
    items:
      type: string
  Catalogue ID:
    type: array
    items:
      type: string
  Name:
    type: array
    items:
      type: string
  Qnt:
    type: array
    items:
      type: integer
  UOM:
    type: array
    items:
      type: string
  Unit_cost:
    type: array
    items:
      type: number
  Total_cost:
    type: array
    items:
      type: number
```
