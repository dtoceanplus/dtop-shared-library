.PHONY: $(wildcard x-*)

all: test-image test-run

test-run:
	$(call image_names,$(@))
	docker run --rm \
	--volume $(PWD):/app \
	--workdir /app \
	${LOCAL_NAME} \
	make x-all

include inc.makefile

#x-all: x-dev-install x-check-cov x-check-pylint x-check-commit
x-all: x-dev-install x-check-cov x-check-commit

test-image:
	$(call image_names,$(@))
	docker build --tag ${LOCAL_NAME} \
	--file Dockerfile .

dev: test-image
	$(call image_names,$(@))
	docker run --rm \
	--interactive --tty \
	--volume $(PWD):/app \
	--workdir /app \
	${LOCAL_NAME} \
	bash

clean: clean-dist clean-images

clean-dist:
	git clean -fxd

clean-images:
	export LOCAL_IMAGE_PREFIX=$(LOCAL_IMAGE_PREFIX) && \
	docker rmi --force $(shell docker images --filter "reference=${LOCAL_IMAGE_PREFIX}-*" --quiet) | true

x-check-commit:
	pipenv run pre-commit run --all-files

x-check-pylint:
	pipenv run pylint dtop_shared_library

x-check-cov:
	pipenv run pytest --cov=dtop_shared_library --cov-fail-under=100 --cov-report term-missing tests

x-dev-install:
	pipenv run python setup.py develop

x-dev-uninstall:
	pipenv run python setup.py develop --uninstall

x-pip-env:
	pipenv install --dev

x-pip-run:
	pipenv shell

x-pip-rm:
	pipenv --rm

x-conda-create: x-conda-rm
	conda create --prefix envs pip make compilers --channel conda-forge

x-conda-rm:
	rm -fr envs
