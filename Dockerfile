FROM continuumio/miniconda3:latest

RUN apt-get update && apt-get install -y gcc libpthread-stubs0-dev
RUN conda config --set always_yes True

RUN conda create --name xxx pip make --channel anaconda
ENV PATH /opt/conda/envs/xxx/bin:${PATH}

RUN pip install pipenv==2018.11.26

WORKDIR /app

COPY Pipfile .
COPY Pipfile.lock .
RUN pipenv install --dev
