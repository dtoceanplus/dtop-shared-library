"""
This function estimates the rao for wave energy convertes

"""
from numpy.linalg import solve
import numpy as np


def response_amplitude_operator(omega, mass, damping, stiffness, f_ex):
    """
    RAO: calculates the response amplitude operator for the device motion,
                            based on the given numerical model coefficients

    Args:
        omega (numpy.ndarray) [rad/s]: wave frequency to discretize numerical model
        mass (numpy.ndarray): Total mass of the isolated WEC [freq, dof, dof]
        damping (numpy.ndarray): Total damping matrix [freq, dof, dof]
        stiffness (numpy.ndarray): Total stiffness matrix [dof, dof]
        f_ex (numpy.ndarray): Excitation force matrix function of the dofs,
                                            directions and wave frequencies

    Returns:
        rao (numpy.ndarray): response amplitude operator for the device motion,
                                        dimensions [frequency, direction, dof]
    """

    n_f, n_d, n_dof = np.shape(f_ex)

    rao = np.zeros((n_f, n_d, n_dof)) + 1j * np.zeros((n_f, n_d, n_dof))
    for freq in range(n_f):
        intrinsic_impedance = (
            -omega[freq] ** 2 * mass[freq, :, :]
            + 1j * omega[freq] * damping[freq, :, :]
            + stiffness
        )

        rao[freq, :, :] = solve(intrinsic_impedance, f_ex[freq, :, :].T).T

    return rao
