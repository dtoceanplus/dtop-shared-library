"""
Function for creating JSON Schema for given example of DataFrame.
"""

from typing import Dict, Union

from pandas import DataFrame
from pandas.io.json import build_table_schema


def schema_from_document(document: Union[DataFrame, Dict]) -> Dict:
    """
    Given a `document` (either a `DataFrame`_ or a dictionary to construct it),
    generates a `JSON Schema`_ against which this document could be successfully validated.

    Internally, it uses `build_table_schema()` as intermediate step of schema generation
    and then converts it from Panda's own schema format to JSON Schema format.

    .. _DataFrame: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html
    .. _JSON Schema: https://json-schema.org/understanding-json-schema/reference/
    """

    # When given a dict, convert it to DataFrame
    if isinstance(document, dict):
        document = DataFrame(document)

    # Generate intermediate schema
    pandas_schema = build_table_schema(document)

    # Start creating final schema
    schema = {"type": "object", "additionalProperties": False, "properties": {}}
    for pandas_field in pandas_schema["fields"]:
        name = pandas_field["name"]
        pandas_type = pandas_field["type"]

        # Declare the field as a homogenic array
        # The type for all items will be then added into `items`
        schema["properties"][name] = {"type": "array", "items": {}}
        item = schema["properties"][name]["items"]

        # Here are conversion for (almost) all types that can be found in intermediate schema
        # See Pandas' function as_json_table_type() for the full list
        if pandas_type == "integer":
            item["type"] = "integer"
        elif pandas_type == "boolean":
            item["type"] = "boolean"
        elif pandas_type == "number":
            item["type"] = "number"
        elif pandas_type == "string":
            item["type"] = "string"

    return schema
