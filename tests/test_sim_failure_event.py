"""
This file contains the test functions.

"""
from dtop_shared_library.sim_failure_event import sim_failure_event


def test_check_input1():
    """
    to test the functionalities in sim_failure_event - case 1
    """
    output1 = sim_failure_event(["Id1", "Id2", "Id3"], [5.86e-4, 7.98e-4, 1.009e-3], 10)
    assert type(output1[1]) == list
    assert len(output1[1]) == 3


def test_check_input2():
    """
    to test the functionalities in sim_failure_event - case 2
    """
    output2 = sim_failure_event(
        ["Id1", "Id2", "Id3"], [5.86e-4, 7.98e-4, 1.009e-3], 10, " "
    )
    assert type(output2[1]) == list
    assert len(output2[1]) == 1


def test_check_input3():
    """
    to test the functionalities in sim_failure_event - case 3
    """
    output3 = sim_failure_event(["Id1", "Id2", "Id3"], [2, 10, 11], 10)
    assert type(output3[1]) == list
    assert len(output3[1]) == 3
